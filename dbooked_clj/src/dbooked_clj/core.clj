(ns dbooked-clj.core
  (:import (java.time LocalDateTime format.DateTimeFormatter)))

(defn parse-datetime [date-str]
  "Parse datetime."
  (LocalDateTime/parse date-str (DateTimeFormatter/ISO_DATE_TIME)))

(defn is-before? [this that]
  "Is this time before that?"
  (.isBefore this that))

(defn is-after? [this that]
  "Is this time after that?"
  (.isAfter this that))

(defn in-between? [time start end]
  "Finds if a time falls between two time events."
  (and
   (is-before? (parse-datetime time) (parse-datetime end))
   (is-after? (parse-datetime time) (parse-datetime start))))
  
(defn overlaps? [{sta1 :sta end1 :end} {sta2 :sta end2 :end}]
  "Parses and compares datetime object."
  (or 
   (in-between? sta1 sta2 end2)
   (in-between? sta2 sta1 end1)))

(defn find-overlaps [coll]
  (loop [[frst & rst] coll event-pairs []]
    (if (empty? rst)
      (vec event-pairs)
      (let [all-overlaps (filter (partial overlaps? frst) rst)
            combo-pairs (map (partial vector frst) all-overlaps)]
        (recur rst (concat event-pairs combo-pairs))))))
