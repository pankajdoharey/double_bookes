(ns dbooked-clj.core-test
  (:require [clojure.test :refer :all]
            [dbooked-clj.core :refer :all]))

(def events-calendar
  [{:sta "2018-06-24T09:10:12" :end "2018-06-24T10:00:00"}
   {:sta "2018-06-24T10:00:00" :end "2018-06-24T10:30:00"}
   {:sta "2018-06-24T10:15:00" :end "2018-06-24T10:45:00"}
   {:sta "2018-06-24T10:30:00" :end "2018-06-24T11:30:00"}
   {:sta "2018-06-24T11:00:00" :end "2018-06-24T12:00:00"}
   {:sta "2018-06-24T12:00:00" :end "2018-06-24T15:00:00"}
   {:sta "2018-06-24T12:30:00" :end "2018-06-24T13:30:00"}
   {:sta "2018-06-24T13:30:00" :end "2018-06-24T14:00:00"}
   {:sta "2018-06-24T14:00:00" :end "2018-06-24T15:00:00"}
   {:sta "2018-06-24T14:30:00" :end "2018-06-24T15:00:00"}
   {:sta "2018-06-24T15:30:00" :end "2018-06-24T15:45:00"}
   {:sta "2018-06-24T15:30:00" :end "2018-06-24T16:00:00"}
   {:sta "2018-06-24T16:00:00" :end "2018-06-24T16:30:00"}
   {:sta "2018-06-24T16:30:00" :end "2018-06-24T17:00:00"}
   {:sta "2018-06-24T17:00:00" :end "2018-06-24T17:30:00"}])

(def sample-overlap
  [[{:sta "2018-06-24T10:00:00" :end "2018-06-24T10:30:00"}
    {:sta "2018-06-24T10:15:00" :end "2018-06-24T10:45:00"}]
   [{:sta "2018-06-24T10:15:00" :end "2018-06-24T10:45:00"}
    {:sta "2018-06-24T10:30:00" :end "2018-06-24T11:30:00"}]
   [{:sta "2018-06-24T10:30:00" :end "2018-06-24T11:30:00"}
    {:sta "2018-06-24T11:00:00" :end "2018-06-24T12:00:00"}]
   [{:sta "2018-06-24T12:00:00" :end "2018-06-24T15:00:00"}
    {:sta "2018-06-24T12:30:00" :end "2018-06-24T13:30:00"}]
   [{:sta "2018-06-24T12:00:00" :end "2018-06-24T15:00:00"}
    {:sta "2018-06-24T13:30:00" :end "2018-06-24T14:00:00"}]
   [{:sta "2018-06-24T12:00:00" :end "2018-06-24T15:00:00"}
    {:sta "2018-06-24T14:00:00" :end "2018-06-24T15:00:00"}]
   [{:sta "2018-06-24T12:00:00" :end "2018-06-24T15:00:00"}
    {:sta "2018-06-24T14:30:00" :end "2018-06-24T15:00:00"}]
   [{:sta "2018-06-24T14:00:00" :end "2018-06-24T15:00:00"}
    {:sta "2018-06-24T14:30:00" :end "2018-06-24T15:00:00"}]])

(deftest test-parse-datetime
  (testing "Test if datetime is parsed correctly."
    (let [ev (:sta (events-calendar 0))]
      (let [pdt (parse-datetime ev)]
        (is (= (.getYear pdt) 2018))
        (is (= (.toString (.getMonth pdt)) "JUNE"))
        (is (= (.getDayOfMonth pdt) 24))
        (is (= (.getYear pdt) 2018))
        (is (= (.getHour pdt) 9))
        (is (= (.getMinute pdt) 10))
        (is (= (.getSecond pdt) 12))))))

(deftest test-event-overlap
  (testing "Find if there is an overlap in two input events."
    (let [ev1 (events-calendar 0) ev2 (events-calendar 1)]
      (is (= (overlaps? ev1 ev2) false)))
    (let [ev1 (events-calendar 5) ev2 (events-calendar 6)]
      (is (= (overlaps? ev1 ev2) true)))
    (let [ev1 (events-calendar 12) ev2 (events-calendar 13)]
      (is (= (overlaps? ev1 ev2) false)))))

(deftest test-find-ovelapping-pairs-visually
 (testing "find pairs by printing to console."
    (println (find-overlaps events-calendar))))

(deftest test-find-ovelapping-pairs
  (testing "find the number of overlapping pairs."
    (is (= (count (find-overlaps events-calendar)) 8))
    (is (= (find-overlaps events-calendar) sample-overlap))))
